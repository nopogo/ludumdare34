﻿using UnityEngine;
using System.Collections;

public class CreditDrawings : MonoBehaviour {

	private DrawLine drawLine;
	// public Color lineColorLarge;
	Vector3 left;
	Vector3 right;

	void Start () {
		Time.timeScale = 1f;
		drawLine = (DrawLine)GameObject.FindObjectOfType<DrawLine>();
		StartCoroutine(DrawThings());
		left = Camera.main.ScreenToWorldPoint(new Vector3(0f, 0.5f, Camera.main.nearClipPlane));
		right = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0.5f, Camera.main.nearClipPlane));
	}


	

	IEnumerator DrawThings(){
		while(true){

			float random_x = Random.Range(left.x, right.x);
			StartCoroutine(drawLine.DrawFractal(random_x, 0f, 90, 5, 0.05f, "tree", true));
			yield return new WaitForSeconds(5f);
		}
		
		
	}
}
