﻿using UnityEngine;
using System.Collections;

public class FlyToScreenCorner : MonoBehaviour {

	private float speed = 10f;
	
	public bool go = false;
	public int dir = 0;
	
	void Update () {
		if(go){
			float step = speed * Time.deltaTime;
			Vector3 target = Camera.main.ViewportToWorldPoint(new Vector3(dir, 0 , Camera.main.nearClipPlane));

			transform.position = Vector3.MoveTowards(transform.position, target, step);
			if(Vector3.Distance(target, transform.position) < 0.1f){
				Destroy(gameObject);
			}
		}
		
	}
}
