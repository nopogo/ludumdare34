﻿using UnityEngine;
using System.Collections;

public class ParalaxBackground : MonoBehaviour {

	private float oldZPosition;

	void Awake(){
		oldZPosition = transform.position.z;
	}

	void Update(){
		Vector3 newPos = new Vector3 ((Camera.main.transform.position.x)/2, (Camera.main.transform.position.y)/2, oldZPosition);

		transform.position = newPos;
	}

}
