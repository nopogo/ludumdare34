﻿using UnityEngine;
using System.Collections;

public class PlanetCloudAnimation : MonoBehaviour {

	
	public Renderer cloudsRenderer;


	public float speedPlanet = 0.5f;
	public float speedClouds = 0.25f;

	void Update(){
		Vector2 offsetPlanet = new Vector2(Time.time * (speedPlanet/2), -Time.time * speedPlanet);

		Vector2 offsetClouds = new Vector2(Time.time * (speedClouds/2), -Time.time * speedClouds);


		GetComponent<Renderer>().material.mainTextureOffset = offsetPlanet;

		cloudsRenderer.material.mainTextureOffset = offsetClouds;
	}
}
