﻿using UnityEngine;
using System.Collections;

public class DrawLine : MonoBehaviour {

	public Material lineMat;
	public Material rootMat;
	public Material treeMat;
	private int canvasIndex = 0;
	private int lineCleanupIndex = 1000;
	private int depth;
	private float deg_to_rad = Mathf.PI / 180.0f;
	

	void Update () {
		LineCleanup();
	}

	void LineCleanup(){
		if(canvasIndex > lineCleanupIndex){
			Destroy(transform.GetChild(0).gameObject);
			lineCleanupIndex +=1;
		}
	}
	public void CreateLine(Vector3 start, Vector3 end, float lineSize, string type="line") {
		GameObject canvas = new GameObject("canvas" + canvasIndex); 
		canvas.transform.parent = transform;
		canvas.transform.rotation = transform.rotation;
		LineRenderer lines = (LineRenderer) canvas.AddComponent<LineRenderer>();
		switch(type){
			case "line":
				lines.material = lineMat;
				break;
			case "tree":
				lines.material = treeMat;
				break;
			case "root":
				lines.material = rootMat;
				break;
		}
		
		// c.a = alpha;
		// lines.material.color = c;
		// lines.material.color = Color.blue;
		lines.useWorldSpace = false;
		lines.SetWidth(lineSize, lineSize);
		lines.SetVertexCount(2);
		lines.SetPosition(0, start);
		lines.SetPosition(1, end);	
		canvasIndex++;
	}




	public IEnumerator DrawFractal(float x1, float y1, int angle, int depth, float lineSize, string type, bool firstUp=false){
		yield return new WaitForSeconds(0.3f);
		float scale = 0.2f;
		if(depth != 0){
			int branchAngle;
			if(firstUp){
				branchAngle = angle;
			}else{
				branchAngle = Random.Range(angle-30, angle+30);
			}
			

			float x2 = x1 + (Mathf.Cos(branchAngle * deg_to_rad) * depth * scale);
			float y2 = y1 + (Mathf.Sin(branchAngle * deg_to_rad) * depth * scale);
			CreateLine(new Vector3(x1, y1, 0f), new Vector3(x2, y2, 0f), (lineSize*depth/5f), type);
			StartCoroutine(DrawFractal(x2, y2, branchAngle - 20, depth - 1, lineSize, type));
			StartCoroutine(DrawFractal(x2, y2, branchAngle + 20, depth - 1, lineSize, type));
		}
	}
}
