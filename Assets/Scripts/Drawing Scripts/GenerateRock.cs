﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(LineRenderer))]
[RequireComponent (typeof(EdgeCollider2D))]
public class GenerateRock : MonoBehaviour {

	//Min 0.1f, max 2f
	private float radius = 0f;
	// private float minRadius = 0.1f;
	// private float maxRadius = 0.3f;

	private float offset_var;
	private int segments;

	LineRenderer line;
	EdgeCollider2D edgeCollider;

	private ColorController colorController;

	void Awake(){
		colorController = (ColorController)GameObject.FindObjectOfType<ColorController>();
	}



    public void DrawRock (float minRadius, float maxRadius){
        line = gameObject.GetComponent<LineRenderer>();
        line.material = colorController.rockMatInstance;
        edgeCollider = gameObject.GetComponent<EdgeCollider2D>();
        radius = Random.Range(minRadius, maxRadius);
        offset_var = radius/10;
       	segments = Random.Range(5, 16);
       	int extraLines = Random.Range(1,10);
        line.SetVertexCount (segments + extraLines);
        line.useWorldSpace = false;
        CreatePoints (extraLines);
        CheckIfCollides();
    }


   
   
    void CreatePoints (int extraLines){	
        float x;
        float y;
        float z = 0f;
        
        Vector2[] edgeArray = new Vector2[segments + extraLines];
       
        float angle = 20f;

        for (int i = 0; i < (segments + extraLines); i++){
        	if(i < segments +1){
        		float offset_x = Random.Range(-offset_var, offset_var);
	        	float offset_y = Random.Range(-offset_var, offset_var);
	    		
				x = Mathf.Sin (Mathf.Deg2Rad * angle) * radius;
				y = Mathf.Cos (Mathf.Deg2Rad * angle) * radius;
				if(i >0 && i < segments){
					x += offset_x;
					y += offset_y;
				}
				line.SetPosition (i, new Vector3(x,y,z) );
				edgeArray[i] = new Vector2(x, y);
				angle += (360f / segments);
			//extra (sily) lines        
			} else{
				Vector2 randomPoint = edgeArray[Random.Range(0, edgeArray.Length)];
				edgeArray[i] = new Vector2(randomPoint.x, randomPoint.y);				
				line.SetPosition (i, new Vector3(randomPoint.x,randomPoint.y,0f) );
			}
        	
        }				
        edgeCollider.points = edgeArray;
    }

	void CheckIfCollides(){
		Collider2D[] list = Physics2D.OverlapCircleAll(new Vector2(transform.position.x, transform.position.y), radius);
		foreach(Collider2D col in list){
			if(col.gameObject != gameObject){
				Destroy(gameObject);
			}
			
		}
	}

	public float GetRadius(){
		return radius;
	}

}
