﻿using UnityEngine;
using UnityEngine.SceneManagement;



public class Camera2DFollow : MonoBehaviour{

    public Transform target;
    public Transform target2;
    public float damping = 1;
    public float lookAheadFactor = 3;
    public float lookAheadReturnSpeed = 0.5f;
    public float lookAheadMoveThreshold = 0.1f;

    private float offsetZ;
    private Vector3 lastTargetPosition;
    private Vector3 currentVelocity;
    private Vector3 lookAheadPos;
    private Vector3 offsetY = new Vector3(0f, -5f, 0f);

    // Use this for initialization

 
    void Start(){
    	if(SceneManager.GetActiveScene().buildIndex == 1){
	        lastTargetPosition = target.position;
	        offsetZ = (transform.position - target.position).z;
	        transform.parent = null;
	    }
    }
    public void SwitchTarget(){
        target = target2;
    }

    // Update is called once per frame
    private void Update(){
      	if(SceneManager.GetActiveScene().buildIndex == 1){
      		if(!LevelEngine.gameOver){
	             // only update lookahead pos if accelerating or changed direction
	            float xMoveDelta = (target.position - lastTargetPosition).x;

	            bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

	            if(updateLookAheadTarget){
	                lookAheadPos = lookAheadFactor*Vector3.right*Mathf.Sign(xMoveDelta);
	            }else{
	                lookAheadPos = Vector3.MoveTowards(lookAheadPos, Vector3.zero, Time.deltaTime*lookAheadReturnSpeed);
	            }

	            Vector3 aheadTargetPos = target.position + lookAheadPos + Vector3.forward*offsetZ + offsetY;

	            
	            

	            Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref currentVelocity, damping);
	            
	            //Never lose target
	            if(target.position.y < newPos.y){
	                newPos.y = target.position.y;
	            }
	            
	            //always follow the x axis directly
	            newPos.x = target.position.x;

	            transform.position = newPos;

	            lastTargetPosition = target.position;
	        }
      	}
        
       
    }
}
