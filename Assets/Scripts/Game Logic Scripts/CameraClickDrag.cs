﻿using UnityEngine;
 
public class CameraClickDrag : MonoBehaviour{

    private float dragSpeed = 20f;
    private Vector3 dragOrigin;

    private float xMin = 0f;
    private float xMax = 20f;

    private float yMin = 0f;
    private float yMax = 10f;
 	
 	private float oldPosZ;
 
    void Awake(){
    	oldPosZ = transform.position.z;
    }

    void Update(){
        if (Input.GetMouseButtonDown(0)){
            dragOrigin = Input.mousePosition;
     	}
 
        if (!Input.GetMouseButton(0)) return;
 
        Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
        Vector3 newPos = new Vector3(Mathf.Clamp(transform.position.x - (pos.x * dragSpeed * Time.deltaTime), xMin, xMax), Mathf.Clamp(transform.position.y - (pos.y * dragSpeed* Time.deltaTime), yMin, yMax), oldPosZ);
 	
        transform.position = newPos;//(move, Space.World);  
    }
 
 
}
