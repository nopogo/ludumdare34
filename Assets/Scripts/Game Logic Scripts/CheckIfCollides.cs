﻿using UnityEngine;
using System.Collections;

public class CheckIfCollides : MonoBehaviour {

	private CircleCollider2D coll;
	private GenerateRock genRock;

	void Awake () {
		genRock = GetComponent<GenerateRock>();
	}

	void Start(){
		//Visual test to see the collider generated
		coll = gameObject.AddComponent<CircleCollider2D>();
		coll.radius = genRock.GetRadius();
		coll.isTrigger = true;
		// Collider2D[] list = Physics2D.OverlapCircleAll(new Vector2(transform.position.x, transform.position.y), genRock.GetRadius());
		// foreach(Collider2D col in list){
		// 	if(col.gameObject != gameObject){
		// 		//If there is a pickup within this rock, destroy the pickup
		// 		if (col.tag == "pickupgreen" || col.tag == "pickupwhite" ){
		// 			Debug.Log("pickup detected");
		// 			Time.timeScale = 0f;
		// 			// Destroy(col.gameObject);
		// 		//If there is a rock within this rock, destroy this rock
		// 		}else{
		// 			Time.timeScale = 0f;
		// 			Debug.Log("rock detected");
		// 			// Destroy(gameObject);
		// 		}				
		// 	}
			
		// }
	}

	void OnTriggerStay2D(Collider2D coll){
		if (coll.tag == "pickupgreen" || coll.tag == "pickupwhite" ){
			Destroy(coll.gameObject);
		}
	}
}
