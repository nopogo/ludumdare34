﻿using UnityEngine;
using System.Collections;

public class ColorController : MonoBehaviour {

	public Material backgroundMat;
	public Material rockMat;
	public MeshRenderer backgroundRenderer;
	public MeshRenderer rockRenderer;
	
	private float duration = 300.0F;
	private Color backgroundColorStart;
	private Color backgroundColorEnd = Color.black;

	private Color rockColorStart;
	private Color rockColorEnd = Color.white;

	private Material backgroundMatInstance;
	
	public Material rockMatInstance;



	void Start(){
		backgroundColorStart = backgroundMat.color;
		rockColorStart = rockMat.color;

		backgroundMatInstance = new Material(backgroundMat);
		backgroundMatInstance.color = backgroundColorStart;
		backgroundRenderer.material = backgroundMatInstance;

		rockMatInstance = new Material(rockMat);
		rockMatInstance.color = rockColorStart;
		// drawLine.lineMat = rockMatInstance;
		// rockRenderer.material = rockMatInstance;
	}

	void Update(){
		float lerp = Time.timeSinceLevelLoad / duration;
		backgroundMatInstance.color = Color.Lerp(backgroundColorStart, backgroundColorEnd, lerp);
		rockMatInstance.color = Color.Lerp(rockColorStart, rockColorEnd, lerp);
	}

}
