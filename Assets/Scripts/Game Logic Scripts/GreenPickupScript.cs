﻿using UnityEngine;
using System.Collections;

public class GreenPickupScript : MonoBehaviour {

	public GameObject particles;
	public GameObject plusTen;

	private CircleCollider2D coll;

	void Awake(){
		coll = GetComponent<CircleCollider2D>();
	}

	public IEnumerator PickedUp(){
		coll.enabled = false;
		// particles.SetActive(false);
		yield return new WaitForSeconds(0.1f);
		plusTen.SetActive(true);
		yield return new WaitForSeconds(0.2f);
		plusTen.SetActive(false);
		gameObject.GetComponent<FlyToScreenCorner>().go = true;
	}
}
