﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelEngine : MonoBehaviour {

	
	public Canvas gameOverCanvas;

	public RootPointControl rootPointControl;
	public GameObject rockPrefab;
	public GameObject pickupWhitePrefab;
	public GameObject pickupGreenPrefab;
	public GameObject objParent;
	public GameObject[] lifecounters;

	
	public bool firstStart = false;
	
	public bool playChime = true;

	private Vector3 playerPosition;
	
	
	private float offset_x = 6.5f;
	private float startSpawnDelay = 2f;
	private float startSpawnDelayPickups = 5f;
	private int nrOfObj = 0;
	private int maxAmountOfObjs = 500;
	private int nrOfLifeCounters = 0;


	public static int lives = 0;
	public static int depth;
	public static int extraPoints = 0;

	public static bool gameOver = false;
	public static bool hasStarted = false;

	private DrawLine drawLine;

	void Awake(){
		Time.timeScale = 0f;
	}

	void Update(){
		if(Input.GetButtonDown("left") || Input.GetButtonDown("right")){
			Time.timeScale = 1f;
		}
		for (var i = 0; i < Input.touchCount; ++i) {
			if(Input.GetTouch(i).phase == TouchPhase.Began){
				Time.timeScale = 1f;
			}
		}
		if(hasStarted){
			CalculateDepth();
		}
	}

	public IEnumerator StartLevel(){
		drawLine = GetComponent<DrawLine>();
		StartCoroutine(drawLine.DrawFractal(0f, 0f, 90, 5, 0.05f, "tree", true));
		yield return new WaitForSeconds(1f);
		hasStarted = true;
		Time.timeScale = 1f;
		firstStart = true;
		StartCoroutine(SpawnPickups());
		StartCoroutine(SpawnRocks());

		//Mobile fix to screen dimming into standby mode when using accelerometer
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		
	}
	

	void CalculateDepth(){
		playerPosition = rootPointControl.gameObject.transform.position;
		depth = (int)Mathf.Floor(playerPosition.y*-1f); 
	}

	IEnumerator SpawnRocks(){
		while(hasStarted){
			float tempSpawnDelay = Mathf.Max(startSpawnDelay - depth/100f , 0.5f);
			yield return new WaitForSeconds(tempSpawnDelay);
			SpawnRock();	
		}
	}

	IEnumerator SpawnPickups(){
		while(hasStarted){
			float tempSpawnDelay = Mathf.Max(startSpawnDelayPickups - depth/100f , 0.5f);
			yield return new WaitForSeconds(tempSpawnDelay);
			int spawnChance = Random.Range(1, 6);
			if(spawnChance == 1){
				SpawnPickup(pickupWhitePrefab);
			} else {
				SpawnPickup(pickupGreenPrefab);
			}	
		}
	}

	void Cleanup(){
		Destroy(objParent.transform.GetChild(0).gameObject);
	}

	void SpawnRock(){
		float xPos = Random.Range(-offset_x, offset_x);
		Vector3 spawnPos = new Vector3(xPos, -10f, 0f);
		spawnPos += playerPosition;
		GameObject go = (GameObject)Instantiate(rockPrefab, spawnPos, Quaternion.identity);
		go.transform.SetParent(objParent.transform);

		//increment rockSize based on depth
		float rockMaxSize  = Mathf.Min(2f + depth/100f, 5f);

		go.GetComponent<GenerateRock>().DrawRock(0.1f, rockMaxSize);

		nrOfObj+=1;
		if(nrOfObj>maxAmountOfObjs){
			Cleanup();
		}
	}

	void SpawnPickup(GameObject obj){
		float xPos = Random.Range(-offset_x, offset_x);
		Vector3 spawnPos = new Vector3(xPos, -10f, 0f);
		spawnPos += playerPosition;

		GameObject go = (GameObject)Instantiate(obj, spawnPos, Quaternion.identity);
		go.transform.SetParent(objParent.transform);
		nrOfObj+=1;
		if(nrOfObj>maxAmountOfObjs){
			Cleanup();
		}
	}

	public void GameOver(){
		if(lives == 0){
			gameOver = true;
			Time.timeScale = 0f;
			gameOverCanvas.gameObject.SetActive(true);
		} else{
			lives -=1;
		}
		
	}

	public void AddLifeCounter(){
		nrOfLifeCounters +=1;
		if(nrOfLifeCounters == 5){
			lives += 1;
			nrOfLifeCounters = 0;
			foreach(GameObject counter in lifecounters){
				counter.SetActive(false);
			}
		} else{
			lifecounters[nrOfLifeCounters-1].SetActive(true);
		}
	}

	public void AddPoints(){
		extraPoints += 10;
	}
}
