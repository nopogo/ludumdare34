﻿using UnityEngine;
using System.Collections;

public class PersistentObject : MonoBehaviour {
	public static PersistentObject Instance{get; private set;}

    private void Awake(){
    	if(Instance != null){
    		DestroyImmediate(gameObject);
    		return;
    	}
    	Instance = this;
    	DontDestroyOnLoad(gameObject);
    }
}
