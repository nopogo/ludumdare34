﻿using UnityEngine;
using System.Collections;

public class RootPointControl : MonoBehaviour {

	
	public AudioSource chime;
	public bool accelerometerBool = true;

	private float smooth = 5.0F;
    private float tiltAngle = 90.0F;
    private float tiltAroundZ = 0;
    private float startSpeed = 1f;
    private float speed;
	private LevelEngine levelEngine;
	private DrawLine drawLine;

	//Draw vars
	public Material lineMat;
	// public Color lineColorLarge;

	private float drawSpeed = 0.1f;
	private Vector3 direction;
	private Vector3 lastEnd = Vector3.zero;
	private float lineSizeLarge = 0.05f;
	
	private int tempAngle = 0;
	private float moveVector = 0;

	void Awake(){
		levelEngine = (LevelEngine)GameObject.FindObjectOfType<LevelEngine>();
		drawLine = (DrawLine)GameObject.FindObjectOfType<DrawLine>();
	
		direction =  new Vector3(0, -drawSpeed, 0);
	}

	void Start(){
		if(PlayerPrefs.HasKey("accelerometer")){
			accelerometerBool = PlayerPrefs.GetInt("accelerometer")==1;
		}
	}

	void Update () {
		//handle drawing the line
		if(transform.position != direction){
			direction = transform.position;
			drawLine.CreateLine(lastEnd, direction, lineSizeLarge);
			lastEnd = direction;

			//keyboard input
			moveVector = Input.GetAxis("Horizontal");

			//if mobile
			if(Application.platform == RuntimePlatform.Android ||Application.platform == RuntimePlatform.IPhonePlayer ){
				if(accelerometerBool == false){
					//touchpad input
					for (var i = 0; i < Input.touchCount; ++i) {
						if(Input.GetTouch(i).phase != TouchPhase.Ended){ //moveVector == 0 && 
							// float Camera.main.pixelWidth/2;
							moveVector = (Input.GetTouch(i).position.x/(Camera.main.pixelWidth/2))-1;
						}
					}
				}else{
					//accelerometer input
					moveVector = Input.acceleration.x*2;
					if(moveVector < -1)
						moveVector = -1;
					if(moveVector>1)
						moveVector = 1;
				}				
			}			
			// Draw the fractal roots
			DrawRandomRoots();
		}
		//handle movement
		if(LevelEngine.hasStarted &&! LevelEngine.gameOver)
			Movement();
	}

	void DrawRandomRoots(){
		if(Random.Range(0,101) == 1 &&  moveVector!= 0f){
			//Reverse shoot angle to player angle
			tempAngle = (int)Mathf.Floor(moveVector * 90f);
			//Roots should always grow down
			if(tempAngle > 0){
				tempAngle = -tempAngle;
			}
			StartCoroutine(drawLine.DrawFractal(lastEnd.x, lastEnd.y, tempAngle, 4, lineSizeLarge, "root"));
		}
	}

	void Movement(){
		tiltAroundZ = moveVector * tiltAngle;
		Quaternion target = Quaternion.Euler(0, 0, tiltAroundZ);
		transform.rotation = Quaternion.Lerp(transform.rotation, target, Time.deltaTime * smooth);
		speed = startSpeed + (-transform.position.y/100f);
		transform.Translate(-transform.up * speed * Time.deltaTime, Space.World);
	}

	void OnCollisionEnter2D(Collision2D coll){
		bool playChime;
		if(PlayerPrefs.HasKey("music")){
		 	playChime = PlayerPrefs.GetInt("music")==1;
		} else{
			playChime = true;
		}
		
		if(coll.gameObject.tag == "pickupwhite"){
			coll.gameObject.GetComponent<FlyToScreenCorner>().go = true;
			if(playChime){
				chime.pitch = Random.Range(0.5f, 1f);
				chime.Play();
			}
			
			levelEngine.AddLifeCounter();

		}else if(coll.gameObject.tag == "pickupgreen"){
			StartCoroutine(coll.gameObject.GetComponent<GreenPickupScript>().PickedUp());
			if(playChime){
				chime.pitch = Random.Range(0.2f, 0.5f);
				chime.Play();
			}
			levelEngine.AddPoints();
		}else{
			coll.gameObject.GetComponent<EdgeCollider2D>().enabled = false;
			levelEngine.GameOver();
		}
		
	}
}
