﻿using UnityEngine;
using System.Collections;

public class SeedScript : MonoBehaviour {
	private LevelEngine levelEngine;
	private Camera2DFollow cameraFollow;
	private LineRenderer line;

	void Awake(){
		levelEngine = (LevelEngine)GameObject.FindObjectOfType<LevelEngine>();
		cameraFollow = (Camera2DFollow)GameObject.FindObjectOfType<Camera2DFollow>();
		line = GetComponent<LineRenderer>();
	}

	IEnumerator OnCollisionEnter2D(Collision2D coll){
		StartCoroutine(levelEngine.StartLevel());
		line.enabled = false;
		// Destroy(this.gameObject);
		yield return new WaitForSeconds(3f);
		cameraFollow.SwitchTarget();
		
	}
}
