﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class BloomToggle : MonoBehaviour {

	

	void Awake(){
		if(PlayerPrefs.HasKey("bloom")){
			Camera.main.GetComponent<BloomOptimized>().enabled = PlayerPrefs.GetInt("bloom")==1;
		}
	}
}
