﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonFunctions : MonoBehaviour {

	public GameObject playButton;
	public GameObject pauzeButton;

	public void LoadScene(int i){
		SceneManager.LoadScene(i);
	}

	public void ExitGame(){
		Application.Quit();
	}

	public void PauzeGame(){
		Time.timeScale = 0;
		playButton.SetActive(true);
		pauzeButton.SetActive(false);
	}

	public void ResumeGame(){
		Time.timeScale = 1;
		playButton.SetActive(false);
		pauzeButton.SetActive(true);
	}
}
