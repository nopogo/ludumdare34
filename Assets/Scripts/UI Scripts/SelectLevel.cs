﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SelectLevel : MonoBehaviour {

	public int level = 1;
	
	void Awake(){
		Time.timeScale = 1f;
	}

	
	void OnMouseDown(){
		SceneManager.LoadScene(level);
	}
}
