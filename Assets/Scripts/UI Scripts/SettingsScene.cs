﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingsScene : MonoBehaviour {

	public AudioSource musicSource;
	// public AudioSource chimeSource;
	public GameObject soundButtonOn;
	public GameObject soundButtonOff;
	public Toggle accelToggle;
	public Toggle bloomToggle;
	// public GameObject settingsMenu;

	// private bool settings = false;
	private bool muteMusic = false;
	// private RootPointControl rootPointControl;
	// private GameEngine gameEngine;


	void Awake(){
		// rootPointControl = (RootPointControl)GameObject.FindObjectOfType<RootPointControl>();	
		// gameEngine = (GameEngine)GameObject.FindObjectOfType<GameEngine>();	
		musicSource = GameObject.FindWithTag("music").GetComponent<AudioSource>();
		UpdateButtons();
	}

	void OnLevelWasLoaded(){
		UpdateButtons();
	}

	void UpdateButtons(){
		if(PlayerPrefs.HasKey("music")){
			muteMusic = PlayerPrefs.GetInt("music")==1;

			if(muteMusic){
				soundButtonOn.SetActive(true);
				soundButtonOff.SetActive(false);
				musicSource.mute = false;
				// gameEngine.playChime = true;
			} else{
				soundButtonOn.SetActive(false);
				soundButtonOff.SetActive(true);
				musicSource.mute = true;
				// gameEngine.playChime = false;
			}
		}

		if(PlayerPrefs.HasKey("accelerometer")){
			accelToggle.isOn = PlayerPrefs.GetInt("accelerometer")==1;
		}
		if(PlayerPrefs.HasKey("bloom")){
			bloomToggle.isOn = PlayerPrefs.GetInt("bloom")==1;
		}
	}

	public void SoundButton(bool pressed=true){		
		muteMusic = !muteMusic;		
		PlayerPrefs.SetInt("music", muteMusic?1:0);
		PlayerPrefs.Save();
		UpdateButtons();
		
	}

	public void ToggleAccelerometor(){
		PlayerPrefs.SetInt("accelerometer", accelToggle.isOn?1:0);
		PlayerPrefs.Save();			
	}

	public void ToggleBloom(){
		PlayerPrefs.SetInt("bloom", bloomToggle.isOn?1:0);
		PlayerPrefs.Save();			
	}
}
