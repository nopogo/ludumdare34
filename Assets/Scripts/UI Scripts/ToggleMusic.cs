﻿using UnityEngine;
using System.Collections;

public class ToggleMusic : MonoBehaviour {

	private AudioSource musicSource;

	void Awake(){
		UpdateMusicPlaying();
	}

	void OnLevelWasLoaded(){
		UpdateMusicPlaying();
	}
	void UpdateMusicPlaying(){
		musicSource = GetComponent<AudioSource>();
		if(PlayerPrefs.HasKey("music")){
			musicSource.mute = PlayerPrefs.GetInt("music")==0;
		}
	}
}
