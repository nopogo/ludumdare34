﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UpdateScore : MonoBehaviour {


	public Text uiScoreText;
	public Text uiLivesText;


	
	void Update () {
		uiScoreText.text = (LevelEngine.depth+LevelEngine.extraPoints).ToString();
		uiLivesText.text = LevelEngine.lives.ToString();
	}
}
